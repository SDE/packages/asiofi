Name: asiofi
Version: 0.5.1
Release: 1%{?dist}
Summary: C++ Boost.Asio language bindings for OFI libfabric

License: LGPLv3
%define github github.com
%define gh_user FairRootGroup
%define gh_repo asiofi
%define gh_repo_url https://%{github}/%{gh_user}/%{gh_repo}
URL: %{gh_repo_url}
Source0: %{name}-%{version}.tar.gz

BuildRequires: asio-devel
BuildRequires: cli11-devel
BuildRequires: cmake
BuildRequires: faircmakemodules
BuildRequires: gcc-c++
BuildRequires: git
BuildRequires: libfabric-devel

%description
asiofi provides C++ Asio language bindings for OFI libfabric.

%global debug_package %{nil}

%prep
%autosetup

%build
cmake -S. -Bbuild \
      -DCMAKE_INSTALL_PREFIX=%{_prefix} \
      -DCMAKE_BUILD_TYPE=Release \
      -DDISABLE_COLOR=ON \
      -DBUILD_TESTING=ON
cmake --build build %{?_smp_mflags}

%install
DESTDIR=%{buildroot} cmake --build build --target install

%files
%license LICENSE
%{_bindir}/afi_*


%package devel
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: asio-devel
Requires: libfabric-devel
Summary: Development files for %{name}

%description devel
This package contains the header files and CMake package for developing against asiofi.

%files devel
%license LICENSE
%{_includedir}/asiofi.hpp
%{_includedir}/%{name}
%dir %{_datadir}/cmake
%dir %{_datadir}/cmake/%{name}-%{version}
%{_datadir}/cmake/%{name}-%{version}/%{name}*.cmake
%{_datadir}/%{name}


%changelog
* Thu Sep 9 2021 Dennis Klein <d.klein@gsi.de> - 0.5.1-1
- Package v0.5.1
* Sun Jun  6 2021 Dennis Klein <d.klein@gsi.de> - 0.5.0-1
- Package v0.5.0
